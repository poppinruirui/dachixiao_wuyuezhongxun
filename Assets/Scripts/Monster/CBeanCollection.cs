﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBeanCollection : MonoBehaviour {

    public Collider2D _colliderMain;
    public SpriteRenderer _srMain;

    List<sBeanInfo> m_lstBeanPos = new List<sBeanInfo>();
    List<Ball> m_lstInteractingBall = new List<Ball>();

    static Vector2 vec2Pivot = new Vector2( 0f, 1f );
    static Vector2 vec2TempPos = new Vector2(0f, 1f);
    static Vector2 vec2TempScale = new Vector2(0f, 0f);

    static Rect rectTemp = new Rect( 0, 0, CMonsterEditor.BEAN_COLLECTION_TEX_SIZE, CMonsterEditor.BEAN_COLLECTION_TEX_SIZE );

    public struct sBeanSyncInfo
    {
        public ushort collection_guid;
        public ushort bean_idx;
        public float fDeadTime;
    };

    public struct sBeanRebornInfo
    {
        public ushort collection_guid;
        public ushort bean_idx;
        public float fDeadTime;
        public float fRebornInterval;
    };

    public struct sBeanInfo
    {
        public Vector2 pos;
        public bool bDead;
        public int nId;
        public Color[] colors;
        public CMonsterEditor.sThornConfig buildin_config;
        public CClassEditor.sThornOfThisClassConfig class_config;

    };


    int m_nCollectionGuid = 0;

    List<BoxCollider2D> m_lstBeanColliders = new List<BoxCollider2D>();

	// Use this for initialization
	void Start () {
        this.gameObject.layer = (int)CLayerManager.eLayerId.monster;
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetCollecionGuid( int nGuid )
    {
        m_nCollectionGuid = nGuid;
    }

    string m_szCollectionGuid = "";
    public void SetCollecionGuid(string szGuid)
    {
        m_szCollectionGuid = szGuid;
    }


    public int GetCollectionGuid()
    {
        return m_nCollectionGuid;
    }

    int m_nIndex = 0;
    public void SetIndex( int nIndex )
    {
        m_nIndex = nIndex;
    }



    void SetBeanColliderEnabled( bool bEnabled )
    {
        for ( int i = 0; i < m_lstBeanColliders.Count; i++ )
        {
            if (bEnabled)
            {
                if ( !m_lstBeanPos[i].bDead )
                {
                    m_lstBeanColliders[i].enabled = bEnabled;
                }
            }
            else
            {
                m_lstBeanColliders[i].enabled = bEnabled;
            }
        }
    }

    bool m_bInit = false;
    void Init()
    {
        Texture2D tex2D = new Texture2D(CMonsterEditor.BEAN_COLLECTION_TEX_SIZE, CMonsterEditor.BEAN_COLLECTION_TEX_SIZE);
        Sprite spr = Sprite.Create(tex2D, rectTemp, vec2Pivot);
        _srMain.sprite = spr;
        _srMain.sprite.texture.SetPixels(0, 0, CMonsterEditor.BEAN_COLLECTION_TEX_SIZE, CMonsterEditor.BEAN_COLLECTION_TEX_SIZE, CMonsterEditor.s_Instance.m_ColorAllEmpty);
        m_bInit = true;
    }

    int m_nBeanCount = 0;
    public void GenerateBean( int nTexPosX, int nTexPosY, ref Color[] colors, float fLocalPosX, float fLocalPosY, ref CMonsterEditor.sThornConfig thorn_buildin_config, ref CClassEditor.sThornOfThisClassConfig thorn_in_class_config)
    {
        PaintBean( nTexPosX, CMonsterEditor.BEAN_COLLECTION_TEX_SIZE - nTexPosY, CMonsterEditor.BEAN_PIC_TEX_SIZE, colors );

        // 顺便生成每个豆子的碰撞体
        BoxCollider2D collider = this.gameObject.AddComponent<BoxCollider2D>();
        vec2TempPos.x = fLocalPosX / this.transform.localScale.x;
        vec2TempPos.y = fLocalPosY / this.transform.localScale.y + 0.01f;
        collider.offset = vec2TempPos;
        vec2TempScale.x = 0.02f;
        vec2TempScale.y = 0.02f;
        collider.size = vec2TempScale;
        collider.isTrigger = true;
        m_lstBeanColliders.Add(collider);

        sBeanInfo bean_info;
        bean_info.pos.x = nTexPosX;
        bean_info.pos.y = CMonsterEditor.BEAN_COLLECTION_TEX_SIZE - nTexPosY;
        bean_info.bDead = false;
        bean_info.nId = m_nBeanCount++;
        bean_info.colors = colors;
        bean_info.buildin_config = thorn_buildin_config;
        bean_info.class_config = thorn_in_class_config;
        m_lstBeanPos.Add(bean_info);
    }

    public CClassEditor.sThornOfThisClassConfig GetBeanClassConfig( int bBeanIdx )
    {
        return m_lstBeanPos[bBeanIdx].class_config;
    }

    public void PaintBean( int x, int y, int nTexSize, Color[] colors )
    {
        if ( !m_bInit )
        {
            Init();
        }


        if ( x + nTexSize > CMonsterEditor.BEAN_COLLECTION_TEX_SIZE )
        {
            x = CMonsterEditor.BEAN_COLLECTION_TEX_SIZE - nTexSize;
        }

        if (y + nTexSize > CMonsterEditor.BEAN_COLLECTION_TEX_SIZE)
        {
            y = CMonsterEditor.BEAN_COLLECTION_TEX_SIZE - nTexSize;
        }
        _srMain.sprite.texture.SetPixels( x, y, nTexSize, nTexSize, colors);
        _srMain.sprite.texture.Apply();
    }



    public Vector3 GetPos()
    {
        return this.transform.position;
    }

    Vector2 m_vecLeftTopPos = new Vector2();
    Vector2 m_vecBottomRight = new Vector2();
    public void SetPos( Vector3 pos )
    {
        this.transform.position = pos;

        m_vecLeftTopPos.x = pos.x - CMonsterEditor.BEAN_COLLECTION_WORLD_SIZE_HALF;
        m_vecLeftTopPos.y = pos.y + CMonsterEditor.BEAN_COLLECTION_WORLD_SIZE_HALF;
        m_vecBottomRight.y = pos.y - CMonsterEditor.BEAN_COLLECTION_WORLD_SIZE_HALF;
    }

    public Vector2 GetBottomRight()
    {
        return m_vecBottomRight;
    }

    public Vector2 GetLeftTopPos()
    {
        return m_vecLeftTopPos;
    }

    public float GetScale()
    {
        return this.transform.localScale.x;
    }

    bool m_bActive = false;
    public void ProcessBallEnter( Ball ball, Collider2D collider )
    {
        m_bActive = true;

        m_lstInteractingBall.Add( ball );
        if ( collider == _colliderMain )
        {
            SetBeanColliderEnabled( true );
        }


        for (  int i = 0; i < m_lstBeanColliders.Count; i++ )
        {
            if (collider == m_lstBeanColliders[i])
            {
                if (!m_lstBeanPos[i].bDead)
                {
                    RemoveBean(i);

                    sBeanInfo info = m_lstBeanPos[i];
                    ball.EatBean(GetCollectionGuid(), i, ref info.buildin_config);
                }
            }
        }

    }

    public void ProcessBallExit(Ball ball)
    {
        m_lstInteractingBall.Remove( ball );
        if (m_lstInteractingBall.Count == 0)
        {
            //CMonsterEditor.s_Instance.RemoveCollection(this);
            SetBeanColliderEnabled(false);

            m_bActive = false;
        }

    }
    

    void SetBeanDeadStatus( int idx, bool status )
    {
        sBeanInfo bean_info = m_lstBeanPos[idx];

        if (status == bean_info.bDead)
        {
            return;
        }

        bean_info.bDead = status;
        m_lstBeanPos[idx] = bean_info; // 数据记得回填


        if (status) // dead 
        {
            //_srMain.sprite.texture.SetPixels((int)bean_info.pos.x, (int)bean_info.pos.y, CMonsterEditor.BEAN_PIC_TEX_SIZE, CMonsterEditor.BEAN_PIC_TEX_SIZE, CMonsterEditor.s_Instance.m_ColorBeanDead);
            //_srMain.sprite.texture.Apply();
            PaintBean((int)bean_info.pos.x, (int)bean_info.pos.y, CMonsterEditor.BEAN_PIC_TEX_SIZE, CMonsterEditor.s_Instance.m_ColorBeanDead);

            m_lstBeanColliders[idx].enabled = false;
        }
        else
        {
            //_srMain.sprite.texture.SetPixels((int)bean_info.pos.x, (int)bean_info.pos.y, CMonsterEditor.BEAN_PIC_TEX_SIZE, CMonsterEditor.BEAN_PIC_TEX_SIZE, bean_info.colors);
            //_srMain.sprite.texture.Apply();
            PaintBean((int)bean_info.pos.x, (int)bean_info.pos.y, CMonsterEditor.BEAN_PIC_TEX_SIZE, bean_info.colors);

            if ( m_bActive )
            {
                m_lstBeanColliders[idx].enabled = true;
            }
        }

    }

    public void RemoveBean( int nBeanIdx )
    {
        SetBeanDeadStatus(nBeanIdx, true);
    }

    public void RebornBean(int nBeanIdx)
    {
        SetBeanDeadStatus(nBeanIdx, false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class CCameraManager : MonoBehaviour {

    public static CCameraManager s_Instance;

    /// <summary>
    /// /UI
    /// </summary>
    public InputField _inputTimeBeforeUp;
    public InputField _inputUpTime;
    public InputField _inputObserverTime;
    public InputField _inputDownTime;
    public InputField _inputRebornSpotScale;
    public InputField _inputRebornSpotBlingSpeed;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempPos2 = new Vector3();

    static Vector3 vecCamerCurPos = new Vector3();
    static Vector3 vecArenaLeftBottom_ScreenCorSys = new Vector3();
    static Vector3 vecArenaRightTop_ScreenCorSys = new Vector3();

    float m_fRebornCamerSize = 30f;

    float m_fUpTime = 1f;
    float m_fDownTime = 1f;
    float m_fDeadWatchTime = 10f;
    float m_fTimeBeforeUp = 1f;
    float m_fRebornSpotScale = 0.1f;
    float m_fRebornSpotBlingSpeed = 1f;

    float m_fUpSizeSpeed = 0;
    float m_fDownSizeSpeed = 0;

    
    Vector3 m_vecUpPosSpeed = new Vector3();
    Vector3 m_vecDownPosSpeed = new Vector3();

    int m_nStatus = 0; // 0 - none  1 - uping  2 - idle   3 - downing

    public ETCJoystick _etc;

    public CRebornSpot _RebornSpot;

    public float m_fScale = 10f;

    public GameObject _uiAll;

    public float m_fHidePlayerNameThreshold = 200f;

    // 视野限制
    public float m_fMaxCamSize = 100f;
    public bool m_bMaxCamLimit = false; 

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}

    static Vector3 s_vecPos1 = new Vector3();
    static Vector3 s_vecPos2 = new Vector3();

    // Update is called once per frame
    void Update () {
        UpdateViewField_WorldCoordinate();

        HideAllPlayerName();

        IsAlreadyMaxSize(); // poppin test
    }

    private void FixedUpdate()
    {
        RebornCameraUpLoop();
        RebornCameraDownLoop();
        ShitWaitLoop1();
    }

    public void OnBtnClick_TestRebornCamera()
    {
        BeginRebornCameraUp( 1 );
    }

    float CalculateAfterRebornCameraSize()
    {
        PlayerAction.s_Instance.DoUpdate();

        Camera.main.transform.position = PlayerAction.s_Instance.balls_center_position;

        bool bSuccess = false;
        for (int i = (int)Camera.main.orthographicSize; i >= 1; i--)
        {
            Camera.main.orthographicSize = i;
            vecTempPos = Camera.main.WorldToScreenPoint(PlayerAction.s_Instance.balls_min_position);
            vecTempPos2 = Camera.main.WorldToScreenPoint(PlayerAction.s_Instance.balls_max_position);

            if (Screen.width <= vecTempPos2.x ||
                 Screen.height <= vecTempPos2.y ||
                 vecTempPos.x <= 0 ||
                 vecTempPos.y <= 0
                )
            {
                bSuccess = true;  
                break;
            }

        } // end for 

        if (!bSuccess)
        {
            Debug.LogError("shit");
            return 0;
        }

        return Camera.main.orthographicSize; 
    }

    public float CalculateRebornCameraSize()
    {
        vecTempPos.x = 0f;
        vecTempPos.y = 0f;
        vecTempPos.z = 0f;
        Camera.main.transform.position = vecTempPos;
        bool bSuccess = false;
        for (int i = 1; i < 1000; i++)
        {
            Camera.main.orthographicSize = i;
            vecArenaLeftBottom_ScreenCorSys = MapEditor.s_Instance.GetArenaLeftBottom_Screen();
            vecArenaRightTop_ScreenCorSys = MapEditor.s_Instance.GetArenaRightTop_Screen();
            if (Screen.width >= vecArenaRightTop_ScreenCorSys.x &&
                 Screen.height >= vecArenaRightTop_ScreenCorSys.y &&
                 vecArenaLeftBottom_ScreenCorSys.x >= 0 &&
                 vecArenaLeftBottom_ScreenCorSys.y >= 0
                )
            {
                bSuccess = true;
                break;
            }

        } // end for 

        if (!bSuccess)
        {
            Debug.LogError("shit");
            return 0;
        }

        return Camera.main.orthographicSize + 20;
    }

    public void BeginRebornCameraUp( int nDeadWaitingTime )
    {
        _uiAll.SetActive( false );

        m_fDeadWatchTime = (float)nDeadWaitingTime;

        float fCameraCurSize = Camera.main.orthographicSize;
        vecCamerCurPos = Camera.main.transform.position;
        m_fRebornCamerSize = CalculateRebornCameraSize();
        Camera.main.orthographicSize = fCameraCurSize;
        Camera.main.transform.position = vecCamerCurPos;

        m_fUpSizeSpeed = ( m_fRebornCamerSize - Camera.main.orthographicSize ) / m_fUpTime;
        m_vecUpPosSpeed = (Vector3.zero - Camera.main.transform.position) / m_fUpTime;
        m_nStatus = 1;

        _etc.gameObject.SetActive( false );
        _RebornSpot.gameObject.SetActive( true );
        _RebornSpot.SetScale( m_fRebornSpotScale * m_fRebornCamerSize);
        _RebornSpot.SetPos(MapEditor.s_Instance.GetRebornSpotRandomPos());
        _RebornSpot.BeginTimeCount( m_fDeadWatchTime );
        _RebornSpot.SetBlingSpeed( m_fRebornSpotBlingSpeed );
    }

    Vector3 m_vecDownDetPos = new Vector3();
    float m_fDownAccelerate = 0f;
    float m_fDownV0 = 0f;
    public void BeginRebornCameraDown()
    {
     
        float fCameraCurSize = Camera.main.orthographicSize;
        vecCamerCurPos = Camera.main.transform.position;
        m_fRebornCamerSize = CalculateAfterRebornCameraSize();
        if (  m_fRebornCamerSize < Main.s_Instance.m_fCamMinSize)
        {
            m_fRebornCamerSize = Main.s_Instance.m_fCamMinSize;
        }



        Camera.main.orthographicSize = fCameraCurSize;
        Camera.main.transform.position = vecCamerCurPos;

        m_fDownSizeSpeed = (Camera.main.orthographicSize  - m_fRebornCamerSize) / m_fDownTime;

        // poppin test
        float s = Camera.main.orthographicSize - m_fRebornCamerSize;
        float t = m_fDownTime;
        m_fDownAccelerate = CyberTreeMath.GetA( s, t );
        m_fDownV0 = CyberTreeMath.GetV0(s, t);

        m_vecDownPosSpeed = ( PlayerAction.s_Instance.balls_center_position - Camera.main.transform.position) / m_fDownTime;
        m_vecDownDetPos = PlayerAction.s_Instance.balls_center_position;

        _RebornSpot.gameObject.SetActive(false);

        m_nStatus = 3;
    }

    void RebornCameraUpLoop()
    {
        if (m_nStatus != 1 )
        {
            return;
        }

        Camera.main.orthographicSize += ( Time.fixedDeltaTime * m_fUpSizeSpeed );

        vecTempPos = Camera.main.transform.position;
        vecTempPos.x += (Time.fixedDeltaTime * m_vecUpPosSpeed.x);
        vecTempPos.y += (Time.fixedDeltaTime * m_vecUpPosSpeed.y);
        Camera.main.transform.position = vecTempPos;


        if (Camera.main.orthographicSize >= m_fRebornCamerSize)
        {
            EndRebornCameraUp();
            return;
        }
    }

    void RebornCameraDownLoop()
    {
        if (m_nStatus != 3)
        {
            return;
        }

        

        float fSize = Camera.main.orthographicSize;

       
        fSize -= (Time.fixedDeltaTime * m_fDownSizeSpeed);
       // m_fDownV0 += m_fDownAccelerate * Time.fixedDeltaTime;
       // Debug.Log("m_fDownV0=" + m_fDownV0);
        if (fSize < m_fRebornCamerSize )
        {
            fSize = m_fRebornCamerSize;
            Camera.main.orthographicSize = fSize;
            EndEndRebornCamera();
            return;
        }
    
        Camera.main.orthographicSize = fSize;

        vecTempPos = Camera.main.transform.position;



               
         vecTempPos.x += (Time.fixedDeltaTime * m_vecDownPosSpeed.x);
         vecTempPos.y += (Time.fixedDeltaTime * m_vecDownPosSpeed.y);

        if (m_vecDownPosSpeed.x >0 )
        {
            if ( vecTempPos.x > m_vecDownDetPos.x )
            {
                vecTempPos.x = m_vecDownDetPos.x;
            }
        }
        else if (m_vecDownPosSpeed.x < 0)
        {
            if (vecTempPos.x < m_vecDownDetPos.x)
            {
                vecTempPos.x = m_vecDownDetPos.x;
            }
        }

        if (m_vecDownPosSpeed.y > 0)
        {
            if (vecTempPos.y > m_vecDownDetPos.y)
            {
                vecTempPos.y = m_vecDownDetPos.y;
            }
        }
        else if (m_vecDownPosSpeed.y < 0)
        {
            if (vecTempPos.y < m_vecDownDetPos.y)
            {
                vecTempPos.y = m_vecDownDetPos.y;
            }
        }

        Camera.main.transform.position = vecTempPos;
         

        



    }

    public void EndRebornCameraUp()
    {
        m_nStatus = 2;
    }

    public void EndEndRebornCamera()
    {
        Camera.main.transform.position = m_vecDownDetPos;

        m_nStatus = 4;
        CtrlMode._zoom_time = 0f;
        m_fShitWaitTimeCount = 0f;

        _etc.gameObject.SetActive(true);
        _uiAll.SetActive(true);
        if (CtrlMode.GetCtrlMode() != CtrlMode.CTRL_MODE_JOYSTICK)
        {
            _etc.activated = false;
        }
        else
        {
            PlayerAction.s_Instance.StopAndMoveToCenter();
        }

    }

    float m_fShitWaitTimeCount = 0f;
    void ShitWaitLoop1()
    {
        if (m_nStatus != 4)
        {
            return;
        }

        
        m_fShitWaitTimeCount += Time.fixedDeltaTime;
        if (m_fShitWaitTimeCount >= 1f)
        {
            CtrlMode._zoom_time = 0.5f;
            m_nStatus = 0;
        }

    }



    public bool IsRebornCameraLooping()
    {
        return m_nStatus > 0;
    }



    public float GetDeadWatchTime()
    {
        return m_fDeadWatchTime;
    }

    public void GenerateRebornSpotParam( XmlNode node )
    {
        m_fTimeBeforeUp = 1f;
        m_fUpTime = 1f;
        
        m_fDownTime = 1f;
        m_fRebornSpotScale = 0.1f;
        m_fRebornSpotBlingSpeed = 1f;



        float val = 0f;
        string[] aryParams;
        if (node != null)
        {
            aryParams = node.InnerText.Split(',');



            for (int i = 0; i < aryParams.Length; i++)
            {
                string szParam = aryParams[i];
                switch (i)
                {
                    case 0:
                        {
                            if (!float.TryParse(szParam, out val))
                            {
                                val = 1f;
                            }
                            m_fTimeBeforeUp = val;
                        }
                        break;

                    case 1:
                        {
                            if (!float.TryParse(szParam, out val))
                            {
                                val = 1f;
                            }
                            m_fUpTime = val;
                        }
                        break;

                    case 2:
                        {
                          // nothing
                        }
                        break;

                    case 3:
                        {
                            if (!float.TryParse(szParam, out val))
                            {
                                val = 1f;
                            }
                            m_fDownTime = val;
                        }
                        break;

                    case 4:
                        {
                            if (!float.TryParse(szParam, out val))
                            {
                                val = 0.1f;
                            }
                            m_fRebornSpotScale = val;
                        }
                        break;

                    case 5:
                        {
                            if (!float.TryParse(szParam, out val))
                            {
                                val = 1f;
                            }
                            m_fRebornSpotBlingSpeed = val;
                        }
                        break;

                } // end switch
            } // end for
        } // end if node != null
        _inputTimeBeforeUp.text = m_fTimeBeforeUp.ToString();
        _inputUpTime.text = m_fUpTime.ToString();
        _inputObserverTime.text = m_fDeadWatchTime.ToString();
        _inputDownTime.text = m_fDownTime.ToString();
        _inputRebornSpotScale.text = m_fRebornSpotScale.ToString();
        _inputRebornSpotBlingSpeed.text = m_fRebornSpotBlingSpeed.ToString();
    }

    public void SaveDeadReborn(XmlDocument xmlDoc, XmlNode node )
    {
        string szContent = m_fTimeBeforeUp + "," +
                           m_fUpTime + "," +
                           m_fDeadWatchTime + "," +
                           m_fDownTime + "," +
                           m_fRebornSpotScale + "," +
                           m_fRebornSpotBlingSpeed;

        StringManager.CreateNode(xmlDoc, node, "DeadReborn", szContent);

    }

    public void OnInputValueChanged_TimeBeforeUp()
    {
        float val = 0f;
        if ( !float.TryParse( _inputTimeBeforeUp.text, out val ) )
        {
            val = 1f;
        }
        m_fTimeBeforeUp = val;
    }

    public void OnInputValueChanged_UpTime()
    {
        float val = 0f;
        if (!float.TryParse(_inputUpTime.text, out val))
        {
            val = 1f;
        }
        m_fUpTime = val;
    }

    public void OnInputValueChanged_ObserverTime()
    {
        float val = 0f;
        if (!float.TryParse(_inputObserverTime.text, out val))
        {
            val = 10f;
        }
        m_fDeadWatchTime = val;
    }

    public void OnInputValueChanged_DownTime()
    {
        float val = 0f;
        if (!float.TryParse(_inputDownTime.text, out val))
        {
            val = 1f;
        }
        m_fDownTime = val;
    }

    public void OnInputValueChanged_RebornSpotScale()
    {
        float val = 0f;
        if (!float.TryParse(_inputRebornSpotScale.text, out val))
        {
            val = 0.1f;
        }
        m_fRebornSpotScale = val;
    }


    public void OnInputValueChanged_RebornSpotBlingSpeed()
    {
        float val = 0f;
        if (!float.TryParse(_inputRebornSpotBlingSpeed.text, out val))
        {
            val = 1f;
        }
        m_fRebornSpotBlingSpeed = val;
    }

    public float GetTimeBeforeUp()
    {
        return m_fTimeBeforeUp;
    }

    //// 视野裁剪相关

    Vector3 m_vecWorldCoorViewLeftBottom = new Vector3();
    Vector3 m_vecWorldCoorViewRightTop = new Vector3();

    // 计算视野的世界坐标范围
    void UpdateViewField_WorldCoordinate()
    {
        vecTempPos.x = 0;
        vecTempPos.y = 0;
        vecTempPos.z = 0;
        m_vecWorldCoorViewLeftBottom = Camera.main.ScreenToWorldPoint(vecTempPos);
        vecTempPos.x = Screen.width;
        vecTempPos.y = Screen.height;
        m_vecWorldCoorViewRightTop = Camera.main.ScreenToWorldPoint(vecTempPos);
    }

    public void GetScreenBorder_WorldCoordinate( ref Vector3 vecLeftBottom, ref Vector3 vecRightTop )
    {
        vecLeftBottom = m_vecWorldCoorViewLeftBottom;
        vecRightTop = m_vecWorldCoorViewRightTop;
    }

    public LineRenderer _lrShitBar;

    // 判断球球是否接近视野边缘了
    // 只需判断队伍边缘的四个球球，不需要遍历每个球球 [to youhua]
    public float m_fViewBorderDistance = 50f;
    public void CheckIfBallNearViewBorder( Vector3 vecCurBallPos, float fRadius, float fDirX, float fDirY, ref bool bNearX, ref bool bNearY, ref float fDisX, ref float fDisY)
    {
        bNearX = false;
        bNearY = false;
        if (!CCameraManager.s_Instance.IsMaxCamLimit())
        {
            return; 
        }

        float fAttackDis = fRadius * CtrlMode.m_fAcctackRadiusMultiple;
        if (fDirX > 0)
        {
            fDisX = vecCurBallPos.x - (m_vecWorldCoorViewRightTop.x - fRadius - m_fViewBorderDistance );

          

            if (fDisX >= 0)
            {
                bNearX = true;
            }
        }
        
        else if (fDirX < 0)
        {
            fDisX = vecCurBallPos.x - (m_vecWorldCoorViewLeftBottom.x + fAttackDis);
            if (fDisX <= 0 )
            {
                bNearX = true;
            }

        }

        if (fDirY > 0)
        {
            if (vecCurBallPos.y >= m_vecWorldCoorViewRightTop.y - fAttackDis)
            {
                bNearY = true;
            }
        }

        else if (fDirY < 0)
        {
            if (vecCurBallPos.y <= m_vecWorldCoorViewLeftBottom.y + fAttackDis)
            {
                bNearY = true;
            }
        }

    }


    public bool CheckIfBallInView( float fPosX, float fPosY, float fRadius )
    {
        if ( ( fPosX - fRadius ) > m_vecWorldCoorViewRightTop.x )
        {
            return false;
        }

        if ((fPosX + fRadius) < m_vecWorldCoorViewLeftBottom.x)
        {
            return false;
        }

        if ((fPosY + fRadius) < m_vecWorldCoorViewLeftBottom.y)
        {
            return false;
        }

        if ((fPosY - fRadius) > m_vecWorldCoorViewRightTop.y)
        {
            return false;
        }

        return true;
    }

    public bool CheckIfDustInView( Vector3 pos )
    {
        if (pos.x > m_vecWorldCoorViewRightTop.x)
        {
            return false;
        }

        if (pos.x < m_vecWorldCoorViewLeftBottom.x)
        {
            return false;
        }

        if (pos.y < m_vecWorldCoorViewLeftBottom.y)
        {
            return false;
        }

        if (pos.y > m_vecWorldCoorViewRightTop.y)
        {
            return false;
        }

        return true;
    }

    public bool CheckIfBallTeamInView( float fLeft, float fBottom, float fRight, float fTop)
    {
        if (fLeft > m_vecWorldCoorViewRightTop.x)
        {
            return false;
        }

        if (fRight < m_vecWorldCoorViewLeftBottom.x)
        {
            return false;
        }

        if (fTop < m_vecWorldCoorViewLeftBottom.y)
        {
            return false;
        }

        if (fBottom > m_vecWorldCoorViewRightTop.y)
        {
            return false;
        }

        return true;
    }

    float m_fLastCamSize = 0f;
    void HideAllPlayerName()
    {
        return;

        if ( m_fLastCamSize < 50f && Camera.main.orthographicSize >= 50f )
        {
            CCheat.s_Instance.SetSomeThingVisible( 10f, 4f, false );
            CPlayerManager.s_Instance.SetSomeThingVisible(10f, 4f, false);
        }
        if (m_fLastCamSize >= 50f && Camera.main.orthographicSize < 50f)
        {
            CCheat.s_Instance.SetSomeThingVisible(10f,4f, true);
            CPlayerManager.s_Instance.SetSomeThingVisible(10f, 4f, true);
        }

        if (m_fLastCamSize < 100f && Camera.main.orthographicSize >= 100f)
        {
            CCheat.s_Instance.SetSomeThingVisible(20f, 10f, false);
            CPlayerManager.s_Instance.SetSomeThingVisible(20f, 10f, false);
        }
        if (m_fLastCamSize >= 100f && Camera.main.orthographicSize < 100f)
        {
            CCheat.s_Instance.SetSomeThingVisible(20f, 10f, true);
            CPlayerManager.s_Instance.SetSomeThingVisible(20f, 10f, true);
        }


        if (m_fLastCamSize < 150f && Camera.main.orthographicSize >= 150f)
        {
            CCheat.s_Instance.SetSomeThingVisible(30f, 20f, false);
            CPlayerManager.s_Instance.SetSomeThingVisible(30f, 20f, false);
        }
        if (m_fLastCamSize >= 150f && Camera.main.orthographicSize < 150f)
        {
            CCheat.s_Instance.SetSomeThingVisible(30f, 20f, true);
            CPlayerManager.s_Instance.SetSomeThingVisible(30f, 20f, true);
        }


        if (m_fLastCamSize < 200f && Camera.main.orthographicSize >= 200f)
        {
            CCheat.s_Instance.SetSomeThingVisible(40f, 30f, false);
            CPlayerManager.s_Instance.SetSomeThingVisible(40f, 30f, false);
        }
        if (m_fLastCamSize >= 200f && Camera.main.orthographicSize < 200f)
        {
            CCheat.s_Instance.SetSomeThingVisible(40f, 30f,true);
            CPlayerManager.s_Instance.SetSomeThingVisible(40f, 30f, true);
        }

        ////

        m_fLastCamSize = Camera.main.orthographicSize;
    }
    //// end 视野裁剪相关

    public Vector3 GetSceenCenter()
    {
        vecTempPos.x = Screen.width / 2;
        vecTempPos.y = Screen.height / 2;
        return Camera.main.ScreenToWorldPoint(vecTempPos);
    }

    public void SetMaxCamSizeLimit( bool bLimit, float fMaxCamSize = 100f )
    {
        m_bMaxCamLimit = bLimit;
        m_fMaxCamSize = fMaxCamSize;
    }

    public bool IsMaxCamLimit()
    {
        return m_bMaxCamLimit;
    }

    public float GetMaxCamSizeLimit()
    {
        return m_fMaxCamSize;
    }

    public bool IsAlreadyMaxSize()
    {
        bool bRet = Camera.main.orthographicSize >= m_fMaxCamSize;
                return bRet;
    }
}

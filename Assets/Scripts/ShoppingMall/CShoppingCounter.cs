﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CShoppingCounter : CCyberTreeListItem {

    static Vector2 vecTempSize = new Vector2();

    ShoppingMallManager.eMoneyType m_eMoneyType;
    int m_nMoneyValue = 0;
    bool m_bBought = false;
    string m_szItemName = "";
    int m_nItemId = 0;
    string m_szItemDesc = "";
    string m_szItemPath = "";

    public Image m_imgBg;
    public Button m_btnBuyAndEquip;  
    public Image m_imgStatus;
    public Image m_imgSkin;
    public Image m_imgMoneyType;
    public Image m_imgMoneyBg;
    public Text m_txtMoneyValue;
    public Text m_txtItemDesc;
    public Text m_txtItemName;


    public enum eItemStatus
    {
        buy,
        equip,
        equipped,
    };

    eItemStatus m_eCurItemStatus = eItemStatus.buy;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSprite( Sprite sprite )
    {
        m_imgSkin.sprite = sprite;
    }

    public Sprite GetSprite()
    {
        return m_imgSkin.sprite;
    }

    public void SetItemPath( string szItemPath )
    {
        m_szItemPath = szItemPath;
    }

    public string GetItemPath()
    {
        return m_szItemPath;
    }


    public void SetMoneyType(ShoppingMallManager.eMoneyType eMoneyType)
    {
        m_eMoneyType = eMoneyType;
        m_imgMoneyType.sprite = ShoppingMallManager.s_Instance.m_aryMoneyTypeSpr[(int)eMoneyType];
    }

    public ShoppingMallManager.eMoneyType GetMoneyType()
    {
        return m_eMoneyType;
    }

    public void SetMoneyValue( int nValue )
    {
        m_nMoneyValue = nValue;
        m_txtMoneyValue.text = "x" + nValue.ToString();
    }

    public int GetMoneyValue()
    {
        return m_nMoneyValue;
    }

    public string GetItemName()
    {
        return m_szItemName;
    }

    public void SetItemName( string szItemName )
    {
        m_szItemName = szItemName;
        m_txtItemName.text = szItemName;
    }

    public void SetItemId( int nItemId )
    {
        m_nItemId = nItemId;
    }

    public int GetItemId()
    {
        return m_nItemId;
    }

    public void SetItemDesc( string szDesc )
    {
        m_szItemDesc = szDesc;
        m_txtItemDesc.text = szDesc;
    }

    public string GetItemDesc()
    {
        return m_szItemDesc;
    }

    public void SetItemStatus( eItemStatus status )
    {
        m_eCurItemStatus = status;
        m_imgStatus.sprite = ShoppingMallManager.s_Instance.m_aryBuyButtonStatus[(int)m_eCurItemStatus];
        if (m_eCurItemStatus == eItemStatus.buy)
        {
            m_imgBg.sprite = ShoppingMallManager.s_Instance.m_sprNotEquiped;
            m_imgMoneyType.gameObject.SetActive(true);
            m_txtMoneyValue.gameObject.SetActive(true);
            m_txtMoneyValue.fontSize = ShoppingMallManager.s_Instance.m_nFontSize_NotBought;

            SetMoneyBg(ShoppingMallManager.s_Instance.m_sprMoneyBg_NotBought, ShoppingMallManager.s_Instance.m_fMoneyBgWidth_NotBought, ShoppingMallManager.s_Instance.m_fMoneyBgHeight);
        }
        else
        {
            m_imgBg.sprite = ShoppingMallManager.s_Instance.m_sprNotEquiped;
            m_imgMoneyType.gameObject.SetActive(false);
            //m_txtMoneyValue.gameObject.SetActive(false);
            m_txtMoneyValue.text = ShoppingMallManager.s_Instance.m_szExpireInfo;
            m_txtMoneyValue.fontSize = ShoppingMallManager.s_Instance.m_nFontSize_Bought;

            SetMoneyBg(ShoppingMallManager.s_Instance.m_sprMoneyBg_Bought, ShoppingMallManager.s_Instance.m_fMoneyBgWidth_Bought, ShoppingMallManager.s_Instance.m_fMoneyBgHeight);
        }

        if (m_eCurItemStatus == eItemStatus.equipped)
        {
            m_imgBg.sprite = ShoppingMallManager.s_Instance.m_sprEquiped;
            
        }
    }

    public void SetSkinById( int nSkinId )
    {
        m_imgSkin.sprite = CSkinManager.LoadSkinById(nSkinId);
    }


    public void OnClickButton_Buy()
    {
        if (m_eCurItemStatus == eItemStatus.buy )
        {
            DoBuy();
        }
        else if (m_eCurItemStatus == eItemStatus.equip)
        {
            DoEquip();
        }
    }

    void DoBuy()
    {
        ShoppingMallManager.s_Instance.ShowConfirmBuyPanel( this );
    }

    void DoEquip()
    {
        ShoppingMallManager.s_Instance.DoEquip(this);
    }

    public void SetMoneyBg( Sprite spr, float fWidth, float fHeight )
    {
        m_imgMoneyBg.sprite = spr;
        vecTempSize.x = fWidth;
        vecTempSize.y = fHeight;
        m_imgMoneyBg.GetComponent<RectTransform>().sizeDelta = vecTempSize;
    }
}

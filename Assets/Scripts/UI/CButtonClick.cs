﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CButtonClick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public CFrameAnimationEffect _effectClick;

    public Image _imgMain;
    public Text _txtTitle;

    public enum eButtonStatus
    {
        common,
        down,
    };

    public Sprite[] _aryButtonStatus;
    public string[] _arySzButtonColors = { "#FFFFFF", "#FFFF12" };
    Color[] _aryButtonColors = new Color[2];

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < _arySzButtonColors.Length; i++)
        {
            Color color = new Color();
            if (ColorUtility.TryParseHtmlString(_arySzButtonColors[i],
                                               out color))
            {
                _aryButtonColors[i] = color;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerDown(PointerEventData evt)
    {
		if (_effectClick) {
			_effectClick.BeginPlay (false);
		}
		CAudioManager.s_Instance.PlayAudio (CAudioManager.eAudioId.e_audio_mouse_click_button);
    }

    public void OnPointerUp(PointerEventData evt)
    {

    }

    public void SetButtonStatusImage( eButtonStatus eStatus )
    {
        _imgMain.sprite = _aryButtonStatus[(int)eStatus];
        _txtTitle.color = _aryButtonColors[(int)eStatus];
    }
}
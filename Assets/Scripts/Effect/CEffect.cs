﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CEffect : MonoBehaviour {

    public float m_fRotateSpeed;
    float m_fRotationZ = 0f;

    static Vector3 vecTempScale = new Vector3();
    static Vector3 vecTempPos = new Vector3();

    public SpriteRenderer _sprMain;
    public Image _imgMain;

    public SpriteRenderer _sprWhite;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public void Rotating( float fRotation )
    {
        this.transform.localRotation = Quaternion.identity;
        this.transform.Rotate(0.0f, 0.0f, fRotation);
    }

    // “变白”特效
    public void BeginWhite()
    {

    }

    void WhiteLoop()
    {

    }

    void EndWhite()
    {

    }
}

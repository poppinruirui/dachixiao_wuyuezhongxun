﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System;
public class StringManager : MonoBehaviour {

    void Awake()
    {
       
    }

    // Use this for initialization
    void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public static bool CheckPolygonNameValid( string val )
	{
		if (val.Length == 0) {
			return false;
		}
		return true;
	}

	public static XmlDocument CreateXmlByText( string szXmlContent, ref XmlNode root )
	{
		XmlDocument myXmlDoc = new XmlDocument();
		myXmlDoc.LoadXml ( szXmlContent );
		root = myXmlDoc.SelectSingleNode("root");
		return myXmlDoc;
	}

	public static byte[] String2Bytes( string str )
	{
		return System.Text.Encoding.Unicode.GetBytes ( str );
	}

	public static string Bytes2String( byte[] bytes, int nStartIndex, int nLength )
	{
		return System.Text.Encoding.Unicode.GetString (bytes, nStartIndex, nLength);
	}

    static byte[] _bytes = null;
    static int _pointer = 0;
    public static void BeginPushData(byte[] bytes)
    {
        _bytes = bytes;
        _pointer = 0;
    }


    public static int GetBlobSize()
    {
        return _pointer;
    }

    public static int GetCurPointerPos()
    {
        return _pointer;
    }

    public static void SetCurPointerPos(int pointer)
    {
        _pointer = pointer;
    }

    public static void BeginPopData(byte[] bytes)
    {
        _bytes = bytes;
        _pointer = 0;
    }

    /* 这两个接口有问题，千万不要用
    public static void PushData_Byte( byte val )
    {
        BitConverter.GetBytes(val).CopyTo(_bytes, _pointer);
        _pointer += sizeof(byte);
    }

    public static byte PopData_Byte()
    {
        byte val = (byte)BitConverter.ToChar(_bytes, _pointer);
        _pointer += sizeof(byte);
        return val;
    }
    */
 
    public static void PushData_Int(int val)
    {
        BitConverter.GetBytes(val).CopyTo(_bytes, _pointer);
        _pointer += sizeof(int);
    }

    public static int PopData_Int()
    {
        int val = BitConverter.ToInt32(_bytes, _pointer);
        _pointer += sizeof(int);
        return val;
    }
    
    public static void PushData_Float( float val)
    {
        BitConverter.GetBytes(val).CopyTo(_bytes, _pointer);
        _pointer += sizeof(float);
    }
    
    public static  float PopData_Float()
    {
        float val = BitConverter.ToSingle(_bytes, _pointer);
        _pointer += sizeof(float);
        return val;
    }

    public static void PushData_Short(short val)
    {
        BitConverter.GetBytes(val).CopyTo(_bytes, _pointer);
        _pointer += sizeof(short);
    }

    public static void Push_Uint(uint val)
    {
        BitConverter.GetBytes(val).CopyTo(_bytes, _pointer);
        _pointer += sizeof(uint);
    }


    public static uint PopData_Uint()
    {
        uint val = BitConverter.ToUInt32(_bytes, _pointer);
        _pointer += sizeof(uint);
        return val;
    }

    public static short PopData_Short()
    {
        short val = BitConverter.ToInt16(_bytes, _pointer);
        _pointer += sizeof(short);
        return val;
    }

    //static Dictionary<int, byte[]> m_dicBlobs = new Dictionary<int, byte[]>();
    static List<byte[]> m_lstBlobs = new List<byte[]>();
    public static void InitBlobs()
    {
        for ( int i = 1; i <= 32; i++ )
        {
            int nSize = i * 32;
            byte[] bytes = new byte[nSize];
            m_lstBlobs.Add(bytes);
        }
    }
    /*
    public static byte[] GetSuitableBlob( int nSize )
    {
        byte[] bytes = new byte[16];
        m_lstBlobs.Add(bytes);
        m_lstBlobs.Add(bytes);
        bytes = new byte[32];
        m_lstBlobs.Add(bytes);
        bytes = new byte[64];
        m_lstBlobs.Add(bytes);
        bytes = new byte[96];
        m_lstBlobs.Add(bytes);
        bytes = new byte[128];
        m_lstBlobs.Add(bytes);
        for ( int i = 0; i < m_lstBlobs.Count; i++ )
        {
            if (m_lstBlobs[i].Length >= nSize)
            {
                return m_lstBlobs[i];
            }
        } // end foreach
        //Debug.LogError("预分配的内存块没找到合适的，动态分配一个");
        int n = nSize / 128 + 1;
        nSize = 128 * n;
        bytes = new byte[nSize];
        m_lstBlobs.Add(bytes);

      

        return bytes;
     }
    */

	public static  void CreateNode(XmlDocument xmlDoc,XmlNode parentNode,string name,string value)  
	{  
		XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, name, null);  
		node.InnerText = value;  
		parentNode.AppendChild(node);  
	}

	public static XmlNode CreateNode(XmlDocument xmlDoc, XmlNode parentNode, string name )  
	{  
		XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, name, null);  
		parentNode.AppendChild(node);  
		return node;
	}
}

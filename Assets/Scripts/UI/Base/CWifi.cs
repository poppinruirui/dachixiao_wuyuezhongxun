﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CWifi : MonoBehaviour {

    public GameObject[] m_aryGrid;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateWifi( float fPingTime )
    {
        if (fPingTime < 50f)
        {
            Show(2);
        }
        else if (fPingTime < 100f)
        {
            Show(1);
        }
        else  
        {
            Show(0);

        }

    }

    void Show( int nLevel )
    {
        for ( int i = 0; i < m_aryGrid.Length; i++ )
        {
            m_aryGrid[i].SetActive( false);
        }

        m_aryGrid[nLevel].SetActive( true );
    }
}
